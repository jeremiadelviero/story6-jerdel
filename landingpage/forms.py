from django import forms


class Status_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
        'invalid': 'Isi input dengan email',
    }
    attrs = {
    }

    body = forms.CharField(label='Add Status', required=True, max_length=300,
                           widget=forms.TextInput(attrs=attrs))
