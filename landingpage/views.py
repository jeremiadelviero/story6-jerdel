from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Status_Form
from .models import Status
from django.utils.timezone import localtime, now

response = {}
active = 'active-page'

# Create your views here.
def index(request):
    status = Status.objects.all()
    response['status'] = status
    response['status_form'] = Status_Form
    response['story6'] = active

    html = 'landingpage/index.html'
    return render(request, html, response)

def profile(request):
    response = {'home': active}

    html = 'profilku/index.html'
    return render(request, html, response)

def about(request):
    response = {'about': active, 'title': 'About'}
    return render(request, 'profilku/about.html', response)

def savestatus(request):
    form = Status_Form(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        response['body'] = request.POST['body']
        status = Status(body=response['body'], date=now)
        status.save()
        return HttpResponseRedirect('/')
    else:
        return HttpResponseRedirect('/')
