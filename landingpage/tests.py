from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve
from .views import index, profile
from .models import Status
from .forms import Status_Form
from django.utils.timezone import now
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


# Create your tests here.
class Story6Test(TestCase):

    def test_landing_page_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_landing_page_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_landing_page_contains_hello_message(self):
        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn("Hello, apa kabar?", html_response)

    def test_model_can_create_new_status(self):
        # Creating a new activity
        Status.objects.create(body='Test lab 6 susah')

        # Retrieving all available activity
        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

    def test_form_validation_for_non_blank_items(self):
        name = "Tester"
        form = Status_Form(data={'body': name, 'date': now})
        self.assertTrue(form.is_valid())

    def test_form_validation_for_blank_items(self):
        form = Status_Form(data={'body': '', 'date': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.error_messages['required'],
            "Tolong isi input ini"
        )

    def test_story6_post_success_and_render_the_result(self):
        test = 'Post Test'
        response_post = Client().post('/save/', {'body': test, 'date': now})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_story6_post_error_and_render_the_result(self):
        test = 'Anonymous'
        self.assertEqual(Client().post('/save/', {'body': ''}).status_code, 302)

        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_str_is_equal_to_body(self):
        test = 'Tester'
        response_post = Client().post('/save/', {'body': test, 'date': now})
        self.assertEqual(response_post.status_code, 302)

        test_status = Status.objects.get(pk=1)
        self.assertEqual(str(test_status), test)


class ProfilkuTest(TestCase):

    def test_profile_page_url_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_profile_page_using_profile_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, profile)

    def test_profile_page_contains_my_name(self):
        response = Client().get('/profile/')
        html_response = response.content.decode('utf8')
        self.assertIn("Jeremia Delviero", html_response)

    def test_about_page_url_is_exist(self):
        response = Client().get('/about/')
        self.assertEqual(response.status_code, 200)

    def test_about_page_contains_my_email(self):
        response = Client().get('/about/')
        html_response = response.content.decode('utf8')
        self.assertIn("jeremiadelviero@gmail.com", html_response)


class Story7FunctionalTest(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story7FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Story7FunctionalTest, self).tearDown()

    def test_input_todo(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)  # use the live server url
        # find the form element
        status = "Coba coba"
        body = selenium.find_element_by_id('id_body')
        submit = selenium.find_element_by_id('submit')

        # Fill the form with data
        body.send_keys(status)

        # submitting the form
        submit.send_keys(Keys.RETURN)

        page = selenium.page_source
        self.assertIn('Coba coba', page)

    def test_layout_and_css(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)

        self.assertEqual('Story 6', selenium.title)

        navbar_brand = selenium.find_element_by_class_name('nama-brand')
        self.assertIn('JerDelviero', navbar_brand.text)
        judul = selenium.find_element_by_class_name('title')
        self.assertIn('Hello, apa kabar?', judul.text)

        navbar = selenium.find_element_by_id('responsive-menu')
        self.assertEqual(navbar.value_of_css_property('background-color'), 'rgba(234, 202, 33, 1)')
        body = selenium.find_element_by_tag_name('body')
        self.assertEqual(body.value_of_css_property('background-color'), 'rgba(240, 238, 193, 1)')
