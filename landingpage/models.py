from django.db import models


class Status(models.Model):
    body = models.CharField(max_length=300)
    date = models.DateTimeField(auto_now_add=True, blank=False)

    def __str__(self):
        return self.body
